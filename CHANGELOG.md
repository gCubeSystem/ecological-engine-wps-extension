This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "ecological-engine-wps-extension"


## [v1.2.0-SNAPSHOT] 

- Updated gcube-bom to 2.4.1-SNAPSHOT


## [v1.1.0] - 2022-08-26

- Updated bom to latest version [#23769]


## [v1.0.5] - 2020-06-10

- Updated for support https protocol [#19423]


## [v1.0.4] - 2016-10-03

- First Release

